use clap::{App, Arg};
use std::fs;
use std::path::Path;

/// Lists files in a given directory.
fn list_files_in_directory(directory: &Path) -> Result<Vec<String>, String> {
    let mut files = Vec::new();
    match fs::read_dir(directory) {
        Ok(paths) => {
            for path in paths {
                match path {
                    Ok(entry) => files.push(entry.path().display().to_string()),
                    Err(e) => return Err(format!("Error reading path: {}", e)),
                }
            }
        },
        Err(e) => return Err(format!("Error reading directory: {}", e)),
    }
    Ok(files)
}

fn main() {
    let matches = App::new("List Files CLI")
        .version("1.0")
        .author("Your Name")
        .about("Lists files in a specified directory")
        .arg(
            Arg::with_name("directory")
                .help("The directory to list files in")
                .default_value(".")
                .index(1),
        )
        .get_matches();

    let directory_path = matches.value_of("directory").unwrap();
    let path = Path::new(directory_path);

    match list_files_in_directory(path) {
        Ok(files) => files.iter().for_each(|file| println!("Name: {}", file)),
        Err(e) => eprintln!("{}", e),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::{self, File};
    use std::io::Write;
    use tempfile::tempdir; // Consider adding the `tempfile` crate for creating temporary directories.

    fn create_test_file(dir: &Path, file_name: &str) {
        let file_path = dir.join(file_name);
        let mut file = File::create(file_path).unwrap();
        writeln!(file, "Test content").unwrap();
    }

    #[test]
    fn list_files_in_non_empty_directory() {
        let dir = tempdir().unwrap();
        create_test_file(dir.path(), "test_file.txt");

        let files = list_files_in_directory(dir.path()).unwrap();
        assert_eq!(files.len(), 1);
        assert!(files.iter().any(|f| f.contains("test_file.txt")));
    }

    #[test]
    fn list_files_in_empty_directory() {
        let dir = tempdir().unwrap();

        let files = list_files_in_directory(dir.path()).unwrap();
        assert!(files.is_empty());
    }

    #[test]
    fn list_files_in_non_existent_directory() {
        let dir = Path::new("/path/that/does/not/exist");

        let result = list_files_in_directory(dir);
        assert!(result.is_err());
    }

    #[test]
    fn list_files_when_path_is_a_file() {
        let dir = tempdir().unwrap();
        create_test_file(dir.path(), "not_a_directory");

        let result = list_files_in_directory(&dir.path().join("not_a_directory"));
        assert!(result.is_err()); // Assuming your implementation returns Err for non-directory paths.
    }
}

