
# IDS721 Spring 2024 Weekly Mini Project 8 Rust Command-Line Tool with Testing

## Requirements
1. Rust command-line tool
2. Data ingestion/processing
3. Unit tests


## Setup
1. Install Rust
2. Start by creating a new binary-based Cargo project and changing into the new directory:
   `cargo new project_name && cd project_name`
3. add the dependencies in `Cargo.toml` file for cli and testing
   ```toml
   [dependencies]
   serde_json = "1.0"
   clap = "2.33.3"
   tempfile = "3"
   ```
4. Implement the logic in `main.rs` file
6. Run the project `cargo run` or `cargo run /path/to/directory`

## Deliverables

### Tool functionality
The core functionality revolves around listing files in a specified directory or the current directory by default. The `clap` crate is utilized for argument parsing, providing a user-friendly command-line interface.

`cargo run .`

![img.png](img.png)

`cargo run src`

![img_1.png](img_1.png)

`cargo run srcc`

![img_2.png](img_2.png)

### Data processing
We can extend data processing capabilities by adding features such as filtering (e.g., by file extension) or sorting (e.g., by modification date or size). However, for simplicity, this example will focus on listing files.

### Testing implementation
Testing in Rust is straightforward, thanks to its built-in test framework. We'll write tests for our functionality to ensure our code works as expected under various conditions.

To write comprehensive unit tests for the Rust CLI tool that lists files in a directory, we'll focus on testing the core functionality encapsulated in the list_files_in_directory function. The goal of these tests is to verify that the function behaves correctly under various conditions, such as when the specified directory:

1. Exists and contains files.
2. Is empty.
3. Does not exist.
4. Is actually a file, not a directory.

`cargo test`

![img_3.png](img_3.png)

## Reference
1. https://www.cargo-lambda.info/guide/getting-started.html
2. https://crates.io/crates/tempfile

